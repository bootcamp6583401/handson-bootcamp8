import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

/**
 * Task2
 */
public class Task2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter your birthdate (YYYY-MM-DD): ");
        String birthdateString = scanner.nextLine();

        LocalDate birthdate = LocalDate.parse(birthdateString);

        int age = calculateAge(birthdate, LocalDate.now());

        System.out.println("Your age is: " + age + " years.");

        scanner.close();
    }

    private static int calculateAge(LocalDate birthdate, LocalDate currentDate) {
        return Period.between(birthdate, currentDate).getYears();
    }
}