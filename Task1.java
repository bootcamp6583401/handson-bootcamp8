import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter the value of Voltage (V), or enter 0 if unknown: ");
        double voltage = scanner.nextDouble();

        System.out.print("Enter the value of Current (I), or enter 0 if unknown: ");
        double current = scanner.nextDouble();

        System.out.print("Enter the value of Resistance (R), or enter 0 if unknown: ");
        double resistance = scanner.nextDouble();

        System.out.print("Enter the value of Power (P), or enter 0 if unknown: ");
        double power = scanner.nextDouble();

        int knownValuesCount = 0;
        knownValuesCount += (voltage != 0) ? 1 : 0;
        knownValuesCount += (current != 0) ? 1 : 0;
        knownValuesCount += (resistance != 0) ? 1 : 0;
        knownValuesCount += (power != 0) ? 1 : 0;

        if (knownValuesCount < 2) {
            System.out.println("There must at least be 2 known values.");
            System.exit(0);
        }

        // Resistance (R)
        if (resistance == 0) {
            if (voltage == 0) {
                resistance = power / Math.pow(current, 2);
            } else if (current == 0) {
                resistance = Math.pow(voltage, 2) / power;
            } else {
                resistance = voltage / current;
            }
        }

        // Current (I)
        if (current == 0) {
            if (voltage == 0) {
                current = Math.sqrt(power / resistance);
            } else if (resistance == 0) {
                current = power / voltage;
            } else {
                current = voltage / resistance;
            }
        }

        // Voltage (V)
        if (voltage == 0) {
            if (resistance == 0) {
                voltage = power / current;
            } else if (current == 0) {
                voltage = Math.sqrt(power * resistance);
            } else {
                voltage = current * resistance;
            }
        }

        // Power (P)
        if (power == 0) {
            if (resistance == 0) {
                power = voltage * current;
            } else if (current == 0) {
                power = Math.pow(voltage, 2) * resistance;
            } else {
                power = Math.pow(current, 2) * resistance;
            }
        }

        System.out.println("\nFINAL VALUES");
        System.out.println("Resistance: " + resistance + "(R)");
        System.out.println("Current: " + current + "(I)");
        System.out.println("Volrage: " + voltage + "(V)");
        System.out.println("Power: " + power + "(P)");

        scanner.close();

    }

}