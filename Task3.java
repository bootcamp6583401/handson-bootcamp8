import java.util.Random;
import java.util.Scanner;

/**
 * Task3
 */
public class Task3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random rand = new Random();

        int attempts = 0;

        int currentGuess;

        int secretNum;
        int currentDistance;
        int previousDistance = 1000;

        boolean play = true;

        while (play) {
            secretNum = rand.nextInt(100) + 1;
            attempts = 0;
            // System.out.println("random number: " + secretNum);
            do {
                System.out.print("Guess a number between 1-100: ");
                currentGuess = Integer.parseInt(scanner.nextLine());
                attempts++;
                currentDistance = Math.abs(secretNum - currentGuess);

                String message = currentDistance > previousDistance ? "Colder" : "Hotter";
                previousDistance = currentDistance;
                System.out.println(message);

            } while (currentGuess != secretNum);

            System.out.print("Winner! Total guess: " + attempts + "\nWould you like to play again? (y/n) ");
            String answer = scanner.nextLine();
            play = answer.toLowerCase().equals("y");
        }
        scanner.close();
    }
}